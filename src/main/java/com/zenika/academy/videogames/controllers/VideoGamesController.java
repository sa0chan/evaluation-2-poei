package com.zenika.academy.videogames.controllers;

import com.zenika.academy.videogames.controllers.representation.VideoGameNameRepresentation;
import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.service.VideoGamesService;
import com.zenika.academy.videogames.service.rawg.RawgDatabaseClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/video-games")
public class VideoGamesController {

    private VideoGamesService videoGamesService;
    private RawgDatabaseClient client;

    @Autowired
    public VideoGamesController(VideoGamesService videoGamesService, RawgDatabaseClient client) {
        this.videoGamesService = videoGamesService;
        this.client = client;
    }

    /**
     * Récupérer la liste des jeux vidéos possédés pas l'utilisateur
     *
     * Exemple :
     *
     * GET /video-games
     */
    @GetMapping
    public List<VideoGame> listOwnedVideoGames() {
        return videoGamesService.ownedVideoGames();
    }

    /**
     * Récupérer un jeu vidéo par son ID
     *
     * Exemple :
     *
     * GET /video-games/3561
     */
    @GetMapping("/{id}")
    public ResponseEntity<Optional<VideoGame>> getOneVideoGame(@PathVariable("id") Long id) {
        Optional<VideoGame> foundVideoGame = this.videoGamesService.getOneVideoGame(id);
        if(foundVideoGame != null) {
            return ResponseEntity.ok(foundVideoGame);
        }
        else {
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * Ajouter un jeu vidéo à la collection de l'utilisateur
     *
     * Exemple :
     *
     * POST /video-games
     * Content-Type: application/json
     *
     * {
     *     "name": "The Binding of Isaac: Rebirth"
     * }
     */
    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public VideoGame addVideoGameByName(@RequestBody VideoGameNameRepresentation videoGameName) {
        return this.videoGamesService.addVideoGame(videoGameName.getName(),client);
    }

    //delete un jeu
    @DeleteMapping("/{id}")
    public String deleteVideoGame(@PathVariable(value = "id") Long id) {
        videoGamesService.deleteVideoGame(id);
        return "le jeu a n° " + id + " a bien été supprimé";

    }

    //jeu par genre
    @GetMapping("?genre=action" )
    public List<VideoGame> listOwnedVideoGamesFilteredByGenre(@PathVariable(value = "action") String genre) {

        Stream<VideoGame> videoGameStream = videoGamesService.ownedVideoGames().stream();
        Stream<VideoGame> videoGameStreamFiltered =
                videoGameStream.filter(videoGame -> videoGame.getGenres().equals(genre));
        // je veux retourner une liste de questiondisplayable... donc creation de la liste
        List<VideoGame> videoGamesListFiltered = videoGameStreamFiltered.collect(Collectors.toList());
        return videoGamesListFiltered ;


    }

}
