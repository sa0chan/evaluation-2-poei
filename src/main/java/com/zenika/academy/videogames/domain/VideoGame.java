package com.zenika.academy.videogames.domain;

import java.util.List;

public class VideoGame {
    private Long id;
    private String name;
    private List<Genre> genres;
    private Boolean finished = false;

    public VideoGame(Long id, String name, List<Genre> genres, Boolean finished) {
        this.id = id;
        this.name = name;
        this.genres = List.copyOf(genres);
        this.finished = finished;
    }

    public VideoGame() {}

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Genre> getGenres() {
        return List.copyOf(genres);
    }

    public void setGenres(List<Genre> genres) {
        this.genres = List.copyOf(genres);
    }
    public Boolean getFinished() {
        return finished;
    }

    public void setFinished(Boolean finished) {
        this.finished = finished;
    }
}
