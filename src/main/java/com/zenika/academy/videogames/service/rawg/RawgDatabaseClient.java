package com.zenika.academy.videogames.service.rawg;

import com.zenika.academy.videogames.domain.VideoGame;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Cette classe set à faire des requêtes HTTP vers l'api de rawg.io (https://rawg.io/apidocs et https://api.rawg.io/docs)
 *
 * Il n'est pas nécéssaire de savoir comment elle marche. Seulement de comprendre comment elle est utilisée.
 */
@Component
public class RawgDatabaseClient {

    private final RestTemplate restTemplate;


    public RawgDatabaseClient() {
        this.restTemplate = new RestTemplate();
    }

    public VideoGame getVideoGameFromName(String name) {
        RawgSearchResponse body = restTemplate
                .getForObject("https://api.rawg.io/api/games?search=" + name, RawgSearchResponse.class);
        if (body != null && body.results.size() > 0) {
            return body.results.get(0);
        }
        else {
            return null;
        }
    }

    private static class RawgSearchResponse {
        private List<VideoGame> results;

        public List<VideoGame> getResults() {
            return results;
        }

        public void setResults(List<VideoGame> results) {
            this.results = results;
        }
    }
}
